# Spring Boot MongoDB CRUD Example

### Thing todo list

1. CLone this repository: `git clone https://gitlab.com/hendisantika/spring-boot-mongodb-crud-example2.git`
2. Navigate to the folder: `cd spring-boot-mongodb-crud-example2`
3. Run the application: `mvn clean spring-boot:run`
4. Check the console/terminal

Article Reference

* https://javatechonline.com/spring-boot-mongodb-crud-example/
