package com.hendisantika.springbootmongodbcrudexample.repository;

import com.hendisantika.springbootmongodbcrudexample.entity.Book2;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import java.util.List;
import java.util.Optional;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-mongodb-crud-example
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 05/05/21
 * Time: 14.43
 */
public interface Book2Repository extends MongoRepository<Book2, Integer> {
//--------------------------------custom query methods------------------------

    @Query("{id :?0}")
        //SQL Equivalent : SELECT * FROM BOOK WHERE ID=?
    Optional<Book2> getBookById(Integer id);

    @Query("{pages : {$lt: ?0}}")
        // SQL Equivalent : SELECT * FROM BOOK where pages<?
        //@Query("{ pages : { $gte: ?0 } }")                        // SQL Equivalent : SELECT * FROM BOOK where
        // pages>=?
        //@Query("{ pages : ?0 }")                                      // SQL Equivalent : SELECT * FROM BOOK where
        // pages=?
    List<Book2> getBooksByPages(Integer pages);

    @Query("{author : ?0}")
        // SQL Equivalent : SELECT * FROM BOOK where author = ?
    List<Book2> getBooksByAuthor(String author);

    @Query("{author: ?0, cost: ?1}")
        // SQL Equivalent : SELECT * FROM BOOK where author = ? and cost=?
        //@Query("{$and :[{author: ?0},{cost: ?1}] }")
    List<Book2> getBooksByAuthorAndCost(String author, Double cost);

    @Query("{$or :[{author: ?0},{name: ?1}]}")
        //SQL Equivalent : select count(*) from book where author=? or name=?
    List<Book2> getBooksByAuthorOrName(String author, String name);

    @Query(value = "{author: ?0}", count = true)
        //SQL Equivalent : select count(*) from book where author=?
    Integer getBooksCountByAuthor(String author);

    //Sorting
    @Query(value = "{author:?0}", sort = "{name:1}")
    //ASC
    //@Query(value = "{author=?0}", sort= "{name:-1}") //DESC
    List<Book2> getBooksByAuthorSortByName(String author);
}
