package com.hendisantika.springbootmongodbcrudexample;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootMongodbCrudExampleApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringBootMongodbCrudExampleApplication.class, args);
    }

}
