package com.hendisantika.springbootmongodbcrudexample.init;

import com.hendisantika.springbootmongodbcrudexample.entity.Book;
import com.hendisantika.springbootmongodbcrudexample.repository.BookRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-mongodb-crud-example
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 04/05/21
 * Time: 23.40
 */
@Component
public class findOperationRunner implements CommandLineRunner {

    @Autowired
    private BookRepository bookRepo;

    @Override
    public void run(String... args) throws Exception {

        // findAll() : It will retireve all records saved into DB
        List<Book> bookList = bookRepo.findAll();
        bookList.forEach(System.out::println);     // Printing all saved books

        Optional<Book> opt = bookRepo.findById("ISBN10:1484240251");
        if (opt.isPresent()) {
            Book b1 = opt.get();
            System.out.println("Here is the book details : " + b1);
        } else {
            System.out.println("Given Id not found");
        }
    }
}